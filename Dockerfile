FROM centos:centos7
ARG dist_url

ENV LC_CTYPE=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    PATH="/opt/ownconda/bin:${PATH}"

# Add own CA
ADD own-ca.pem /etc/pki/ca-trust/source/anchors/
RUN update-ca-trust

# Build dependencies
RUN echo "Set locale" \
 && localedef -c -i en_US -f UTF-8 en_US.UTF-8 \
 && echo "Dbus machine-id was empty in the Docker image" \
 && /bin/dbus-uuidgen > /etc/machine-id \
 && echo "Basic setup" \
 && yum install -y deltarpm epel-release wget bzip2

# Install updates
RUN yum upgrade -y

# Install required system libraries
# (qt5 libs are only avialable in the develop image)
RUN echo "Basic libs" \
 && yum install -y \
    glibc glib2 libgcc libstdc++ \
 && echo "krb5" \
 && yum install -y keyutils \
 && echo "mariadb-connector-c" \
 && yum install -y curl \
 && echo "openblas" \
 && yum install -y libgfortran

# Ownconda Python Distribution
RUN wget --quiet $dist_url -O ownconda.sh \
 && bash ./ownconda.sh -b -p /opt/ownconda \
 && rm ownconda.sh \
 && /opt/ownconda/bin/conda update --all --yes --quiet \
 && /opt/ownconda/bin/conda clean --all --yes --quiet
